# Chatatan

[![pipeline status](https://gitlab.com/chatatan-bot/chatatan/badges/master/pipeline.svg)](https://gitlab.com/chatatan-bot/chatatan/-/commits/master)
[![coverage report](https://gitlab.com/chatatan-bot/chatatan/badges/master/coverage.svg)](https://gitlab.com/chatatan-bot/chatatan/-/commits/master)

## About Chatatan

Chatatan is a LINE bot application that serves as a personal notes and schedule keeper. Check out our Chatatan LINE bot!

<a href="https://line.me/R/ti/p/%40045nhzyf"><img height="36" alt="Add Chatatan" src="https://scdn.line-apps.com/n/line_add_friends/btn/en.png"></a>

## Features

Chatatan Incorporated (Inc.) is one of the four services in Chatatan. It acts as the handler which receives, processes, and responds to triggers from LINE. It serves as the service that bridges our LINE users to their stored data in our API endpoints.

Chatatan offers two main features: Notes and Schedule. However, both of these features have many sub-features:

|No|Feature|Desc|
|---|---|---|
|1|`Add Event`| Add event to the user's schedule. Users should input the event's `name`, `start time`, `end time`, and if it is a `routine` occasion or not. Non-routine occasions include family gatherings, a flight or train trip, while weekly (routine) events include classes and organization meetings. Users will be given the option to input either the `date` or `day` of event. |
|2|`Edit Event`| Allow users to edit the `start time`, `end time`, or `date` of an event. | 
|3|`Delete Event`| Remove an event from the user's schedule. |
|4|`See Schedule`| Display the user's schedule sorted by `day` (Monday to Sunday) and `start time`. Users could see their schedule for `This week`, `Next Week`, or `Routine` to show weekly events only. |
|5|`Search`| Search all events that happen on a particular date. |
|6|`Share Schedule`| Share schedule details from point 4 in the form of text to the user's friends or groups. |
|7|`Write Note`| Write a note containing any information the user wishes to write, such as meeting notes, plans, and etc. |
|8|`See All Notes`| Display all the user's notes. |
|9|`Delete Note`| Remove an unwanted or unnecessary note from a user. |
|10|`Share Note`| Share a specific note as wanted by the user to the user's friends or groups. |

## Authors [B10]:
* [Wulan Mantiri](https://gitlab.com/wulanmantiri_)
* [Naufal Hilmi Irfandi](https://gitlab.com/naufalirfandi)
* [Mohammad Hasan Albar](https://gitlab.com/muhammadalbr)
* [Gilbert Stefano Wijaya](https://gitlab.com/gilbertstefano48)
* [Bayukanta Iqbal Gunawan](https://gitlab.com/bayukanta)

## Acknowledgements
* CS UI - Advanced Programming B 2020