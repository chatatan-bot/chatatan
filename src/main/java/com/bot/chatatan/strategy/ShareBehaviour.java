package com.bot.chatatan.strategy;

public interface ShareBehaviour extends Strategy {
    String share(String extendedUrl);
}