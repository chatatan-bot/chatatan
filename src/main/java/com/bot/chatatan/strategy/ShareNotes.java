package com.bot.chatatan.strategy;

import com.bot.chatatan.utilities.ApiFetcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("shareNotes")
public class ShareNotes implements ShareBehaviour {

    @Autowired
    ApiFetcher fetch;

    public String share(String id) {
        fetch.setBaseUrl("notes");
        return fetch.getString("retrieveDetails/" + id);
    }

    @Override
    public String getName() {
        return "shareNotes";
    }

}