package com.bot.chatatan.strategy;

import org.json.JSONObject;

public interface DeleteBehaviour extends Strategy {
    String delete(JSONObject data);
}