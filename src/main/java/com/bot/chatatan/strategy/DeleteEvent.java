package com.bot.chatatan.strategy;

import com.bot.chatatan.utilities.ApiFetcher;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component("deleteEvent")
public class DeleteEvent implements DeleteBehaviour {

    @Autowired
    ApiFetcher fetch;

    public String delete(JSONObject data) {
        fetch.setBaseUrl("schedule");
        ResponseEntity<String> response = fetch.delete("event", data);
        return response.getBody();
    }

    @Override
    public String getName() {
        return "deleteEvent";
    }
    
}