package com.bot.chatatan.strategy;

public interface Strategy {
    String getName();
}