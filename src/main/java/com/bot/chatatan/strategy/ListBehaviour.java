package com.bot.chatatan.strategy;

import java.util.List;
import org.json.JSONObject;

public interface ListBehaviour extends Strategy {
    List<JSONObject> list(String userId);
}