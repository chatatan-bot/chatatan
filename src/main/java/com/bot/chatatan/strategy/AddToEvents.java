package com.bot.chatatan.strategy;

import com.bot.chatatan.utilities.ApiFetcher;
import com.bot.chatatan.utilities.JsonCreator;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component("addToEvents")
public class AddToEvents implements AddBehaviour {

    @Autowired
    ApiFetcher fetch;

    public ResponseEntity<Void> add(String userId, String[] input) {
        JSONObject event = JsonCreator.createEvent(userId, input);
        fetch.setBaseUrl("schedule");
        return fetch.post("event", event);
    }

    @Override
    public String getName() {
        return "addToEvents";
    }
    
}