package com.bot.chatatan.strategy;

import com.bot.chatatan.utilities.ApiFetcher;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("listSchedule")
public class ListSchedule implements ListBehaviour {

    @Autowired
    private ApiFetcher fetch;

    public List<JSONObject> list(String extendedUrl) {
        fetch.setBaseUrl("schedule");
        return fetch.getList(extendedUrl);
    }

    @Override
    public String getName() {
        return "listSchedule";
    }

}