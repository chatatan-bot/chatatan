package com.bot.chatatan.strategy;

import org.springframework.http.ResponseEntity;

public interface AddBehaviour extends Strategy {
    ResponseEntity<Void> add(String userId, String[] input);
}