package com.bot.chatatan.strategy;

import com.bot.chatatan.utilities.ApiFetcher;
import com.bot.chatatan.utilities.JsonCreator;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component("addToNotes")
public class AddToNotes implements AddBehaviour {

    @Autowired
    ApiFetcher fetch;

    public ResponseEntity<Void> add(String userId, String[] input) {
        JSONObject notes = JsonCreator.createNotes(userId, input);
        fetch.setBaseUrl("notes");
        return fetch.post("notes", notes);
    }

    @Override
    public String getName() {
        return "addToNotes";
    }
    
}