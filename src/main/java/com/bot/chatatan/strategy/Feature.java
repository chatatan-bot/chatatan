package com.bot.chatatan.strategy;

import com.bot.chatatan.utilities.ApiFetcher;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component("feature")
public class Feature {

    @Autowired
    private ApiFetcher fetch;

    private AddBehaviour addBehaviour;
    private ListBehaviour listBehaviour;
    private DeleteBehaviour deleteBehaviour;
    private ShareBehaviour shareBehaviour;

    private Map<String, AddBehaviour> addBehaviours;
    private Map<String, ListBehaviour> listBehaviours;
    private Map<String, DeleteBehaviour> deleteBehaviours;
    private Map<String, ShareBehaviour> shareBehaviours;

    @Autowired
    public Feature(
        @Qualifier("addToEvents") AddBehaviour addToEvents,
        @Qualifier("listSchedule") ListBehaviour listSchedule,
        @Qualifier("deleteEvent") DeleteBehaviour deleteEvent,
        @Qualifier("shareSchedule") ShareBehaviour shareSchedule,
        @Qualifier("addToNotes") AddBehaviour addToNotes,
        @Qualifier("listNotes") ListBehaviour listNotes,
        @Qualifier("deleteNotes") DeleteBehaviour deleteNotes,
        @Qualifier("shareNotes") ShareBehaviour shareNotes) {

        this.addBehaviours = new HashMap<>();
        this.addBehaviours.put(addToEvents.getName(), addToEvents);
        this.addBehaviours.put(addToNotes.getName(), addToNotes);
        
        this.listBehaviours = new HashMap<>();
        this.listBehaviours.put(listSchedule.getName(), listSchedule);
        this.listBehaviours.put(listNotes.getName(), listNotes);

        this.deleteBehaviours = new HashMap<>();
        this.deleteBehaviours.put(deleteEvent.getName(), deleteEvent);
        this.deleteBehaviours.put(deleteNotes.getName(), deleteNotes);

        this.shareBehaviours = new HashMap<>();
        this.shareBehaviours.put(shareSchedule.getName(), shareSchedule);
        this.shareBehaviours.put(shareNotes.getName(), shareNotes);
    }

    public ResponseEntity<Void> add(String userId, String[] input) {
        return this.addBehaviour.add(userId, input);
    }

    public List<JSONObject> list(String extendedUrl) {
        return this.listBehaviour.list(extendedUrl);
    }

    public String delete(JSONObject data) {
        return this.deleteBehaviour.delete(data);
    }

    public String share(String extendedUrl) {
        return this.shareBehaviour.share(extendedUrl);
    }

    public void setAddBehaviour(String addBehaviour) {
        this.addBehaviour = addBehaviours.get(addBehaviour);
    }

    public AddBehaviour getAddBehaviour() {
        return this.addBehaviour;
    }

    public void setListBehaviour(String listBehaviour) {
        this.listBehaviour = listBehaviours.get(listBehaviour);
    }

    public ListBehaviour getListBehaviour() {
        return this.listBehaviour;
    }

    public void setDeleteBehaviour(String deleteBehaviour) {
        this.deleteBehaviour = deleteBehaviours.get(deleteBehaviour);
    }

    public DeleteBehaviour getDeleteBehaviour() {
        return this.deleteBehaviour;
    }

    public void setShareBehaviour(String shareBehaviour) {
        this.shareBehaviour = shareBehaviours.get(shareBehaviour);
    }

    public ShareBehaviour getShareBehaviour() {
        return this.shareBehaviour;
    }

    /*
     * Methods for Schedule Feature
     */
    public ResponseEntity<Void> follow(String userId) {
        return fetch.post("follow/" + userId);
    }

    public ResponseEntity<Void> updateSchedule(String userId) {
        return fetch.post("update/" + userId);
    }

    public List<JSONObject> search(String date, String userId) {
        String extendedUrl = String.format("search/%s/%s", date, userId);
        fetch.setBaseUrl("schedule");
        return fetch.getList(extendedUrl);
    }

    public List<Object> edit(JSONObject event) {
        return fetch.put(event).getBody();
    }

}
