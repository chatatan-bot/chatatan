package com.bot.chatatan.strategy;

import com.bot.chatatan.utilities.ApiFetcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("shareSchedule")
public class ShareSchedule implements ShareBehaviour {

    @Autowired
    ApiFetcher fetch;

    public String share(String extendedUrl) {
        fetch.setBaseUrl("schedule");
        return fetch.getString("details/" + extendedUrl);
    }

    @Override
    public String getName() {
        return "shareSchedule";
    }

}