package com.bot.chatatan.strategy;

import com.bot.chatatan.utilities.ApiFetcher;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("listNotes")
public class ListNotes implements ListBehaviour {

    @Autowired
    private ApiFetcher fetch;

    public List<JSONObject> list(String userId) {
        fetch.setBaseUrl("notes");
        return fetch.getList("retrieve/" + userId);
    }

    @Override
    public String getName() {
        return "listNotes";
    }

}