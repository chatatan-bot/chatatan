package com.bot.chatatan.flexcontainers;

import static java.util.Arrays.asList;

import com.bot.chatatan.utilities.JsonCreator;
import com.linecorp.bot.model.action.DatetimePickerAction;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class ScheduleGuide implements Supplier<FlexMessage> {

    @Override
    public FlexMessage get() {
        final Box bodySection = createBodySection(); 
        final Box footerSection = createFooterSection();
        final Bubble bubble =
            Bubble.builder()
                .body(bodySection)
                .footer(footerSection)
                .build();
        return new FlexMessage("Guide for Schedule", bubble);
    }

    private Box createBodySection() {
        List<FlexComponent> body = new ArrayList<>();

        body.add(Text.builder()
                    .text("Guide for Schedule")
                    .color("#182D36")
                    .size(FlexFontSize.LG)
                    .align(FlexAlign.CENTER)
                    .weight(TextWeight.BOLD)
                    .build());
                    
        body.add(Separator.builder().color("#182D36").build());

        body.add(descriptionText("Add event by texting with this format:"));
        body.add(addInputText());

        body.add(descriptionText("See your schedule"));
        body.add(buttonLayout("LISTSCHEDULE"));

        body.add(descriptionText("Search events on a specific date"));
        body.add(searchButton());

        body.add(descriptionText("Share your schedule to friends/groups"));
        body.add(buttonLayout("SHARESCHEDULE"));

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(body)
                .build();
    }

    private Text descriptionText(String text) {
        return Text.builder()
                .text(text)
                .color("#182D36")
                .size(FlexFontSize.SM)
                .weight(TextWeight.BOLD)
                .align(FlexAlign.START)
                .margin(FlexMarginSize.LG)
                .wrap(true)
                .build();
    }

    private Text addInputText() {
        String output = "Add \n[Event Name] \nhh:mm (Start time) \nhh:mm (End time) \n";
        output += "yyyy-mm-dd (Date) / [ex:tuesday] (Day)\nyes/no (is it routine?)";
        return Text.builder()
                .text(output)
                .color("#a3728b")
                .size(FlexFontSize.SM)
                .align(FlexAlign.START)
                .margin(FlexMarginSize.MD)
                .wrap(true)
                .build();
    }

    private Box buttonLayout(String command) {
        Box bottomBox = Box.builder()
                        .layout(FlexLayout.HORIZONTAL)
                        .spacing(FlexMarginSize.SM)
                        .contents(asList(
                            buttonWithDetail("Next week", command),
                            buttonWithDetail("Routine", command)
                        ))
                        .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(asList(
                    buttonWithDetail("This week", command),
                    bottomBox
                ))
                .build();
    }

    private Button buttonWithDetail(String label, String command) {
        return Button.builder()
                .action(PostbackAction.builder()
                        .label(label)
                        .data(JsonCreator.createAction(command, label).toString())
                        .build())
                .style(Button.ButtonStyle.SECONDARY)
                .flex(1)
                .gravity(FlexGravity.CENTER)
                .color("#DBB9CA")
                .build();
    }
    
    private Button searchButton() {
        return Button.builder()
                .action(DatetimePickerAction.OfLocalDate.builder()
                        .label("Search")
                        .initial(LocalDate.now())
                        .min(LocalDate.now())
                        .data(JsonCreator.createButton("SEARCH").toString())
                        .build())
                .style(Button.ButtonStyle.SECONDARY)
                .flex(0)
                .gravity(FlexGravity.CENTER)
                .color("#DBB9CA")
                .build();
    }

    private Box createFooterSection() {
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(asList(
                    Button.builder()
                    .action(PostbackAction.builder()
                            .label("Back")
                            .data(JsonCreator.createButton("BACK").toString())
                            .build())
                    .flex(0)
                    .gravity(FlexGravity.CENTER)
                    .build())
                ).build();
    }

}