package com.bot.chatatan.flexcontainers;

import static java.util.Arrays.asList;

import com.bot.chatatan.utilities.JsonCreator;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class NotesGuide implements Supplier<FlexMessage> {

    @Override
    public FlexMessage get() {
        final Box bodySection = createBodySection(); 
        final Box footerSection = createFooterSection();
        final Bubble bubble =
            Bubble.builder()
                .body(bodySection)
                .footer(footerSection)
                .build();
        return new FlexMessage("Guide for Notes", bubble);
    }

    private Box createBodySection() {
        List<FlexComponent> body = new ArrayList<>();

        body.add(Text.builder()
                    .text("Guide for Notes")
                    .color("#182D36")
                    .size(FlexFontSize.LG)
                    .align(FlexAlign.CENTER)
                    .weight(TextWeight.BOLD)
                    .build());
        body.add(Separator.builder().color("#182D36").build());
        
        body.add(descriptionText("Write notes with this format:"));
        body.add(inputText("Write\n[Title]\n[Content]"));
        body.add(button("See All Notes", "LISTNOTES"));

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(body)
                .build();
    }

    private Text descriptionText(String text) {
        return Text.builder()
                .text(text)
                .color("#a3728b")
                .size(FlexFontSize.SM)
                .weight(TextWeight.BOLD)
                .align(FlexAlign.START)
                .margin(FlexMarginSize.LG)
                .wrap(true)
                .build();
    }

    private Text inputText(String text) {
        return Text.builder()
                .text(text)
                .color("#182D36")
                .size(FlexFontSize.SM)
                .align(FlexAlign.START)
                .margin(FlexMarginSize.MD)
                .wrap(true)
                .build();
    }

    private Button button(String label, String command) {
        return Button.builder()
                .action(PostbackAction.builder()
                        .label(label)
                        .data(JsonCreator.createButton(command).toString())
                        .build())
                .style(Button.ButtonStyle.SECONDARY)
                .flex(0)
                .gravity(FlexGravity.CENTER)
                .margin(FlexMarginSize.LG)
                .color("#DBB9CA")
                .build();
    }
    
    private Box createFooterSection() {
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(asList(
                    Button.builder()
                    .action(PostbackAction.builder()
                            .label("Back")
                            .data(JsonCreator.createButton("BACK").toString())
                            .build())
                    .flex(0)
                    .gravity(FlexGravity.CENTER)
                    .build())
                ).build();
    }
}

