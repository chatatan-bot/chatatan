package com.bot.chatatan.flexcontainers;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Supplier;
import org.json.JSONObject;

public class SearchOutputFlex implements Supplier<FlexMessage> {

    private List<JSONObject> events;
    private String date;
    private String day;

    public SearchOutputFlex(List<JSONObject> events, String date) {
        this.events = events;
        this.date = date;
        this.day = LocalDate.parse(date).getDayOfWeek().toString();
    }

    @Override
    public FlexMessage get() {
        Bubble bubble = new DayBubble(day, date, events).get();
        String label = "Events on " + date;
        return new FlexMessage(label, bubble);
    }
}