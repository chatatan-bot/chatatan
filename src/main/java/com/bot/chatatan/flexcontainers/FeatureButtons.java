package com.bot.chatatan.flexcontainers;

import com.bot.chatatan.utilities.JsonCreator;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class FeatureButtons implements Supplier<Box> {

    @Override
    public Box get() {
        List<FlexComponent> body = new ArrayList<>();

        body.add(featureButton("Notes"));
        body.add(featureButton("Schedule"));

        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .spacing(FlexMarginSize.SM)
                .margin(FlexMarginSize.MD)
                .contents(body)
                .build();
    }

    private static Button featureButton(String feature) {
        String postbackCommand = "HELP" + feature.toUpperCase();
        return Button.builder()
                .action(helpPostback(feature, postbackCommand))
                .style(Button.ButtonStyle.PRIMARY)
                .gravity(FlexGravity.CENTER)
                .color("#182D36")
                .build();
    }

    private static PostbackAction helpPostback(String label, String command) {
        return PostbackAction.builder()
            .label(label)
            .data(JsonCreator.createButton(command).toString())
            .build();
    }
    
}
