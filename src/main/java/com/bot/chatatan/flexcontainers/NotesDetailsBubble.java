package com.bot.chatatan.flexcontainers;

import static java.util.Arrays.asList;

import com.bot.chatatan.utilities.JsonCreator;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.action.URIAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.net.URI;
import java.net.URLEncoder;
import java.util.function.Supplier;
import org.json.JSONObject;

public class NotesDetailsBubble implements Supplier<FlexMessage> {

    private String note;
    private String title;
    private String content;
    private JSONObject data;

    public NotesDetailsBubble(String note, JSONObject data) {
        this.note = note;
        this.title = data.getString("title");
        this.content = data.getString("notes");
        this.data = data;
    }

    @Override
    public FlexMessage get() {
        Box noteName = createHeaderSection();
        Box noteBody = createBodyBlock();
        Bubble bubble = Bubble.builder()
                .header(noteName)
                .body(noteBody)
                .build();

        return new FlexMessage("Note: " + title, bubble);
    }

    private Box createHeaderSection() {

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .backgroundColor("#182D36")
                .contents(Text.builder()
                        .text(title)
                        .color("#DBB9CA")
                        .size(FlexFontSize.LG)
                        .align(FlexAlign.CENTER)
                        .weight(TextWeight.BOLD)
                        .build())
                .build();
    }

    private Box createBodyBlock() {

        Box placeholderContent = Box
                .builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(asList(
                        Text.builder()
                                .text(content)
                                .color("#182D36")
                                .size(FlexFontSize.Md)
                                .flex(1)
                                .wrap(true)
                                .build()
                ))
                .build();

        Button shareButton = Button.builder().build();
        try {
            URI mobileUri = new URI("line://msg/text" 
                + URLEncoder.encode(note, "UTF-8").replace("+", "%20"));
            URI desktopUri = new URI("https://www.google.com/");
            URIAction shareUriAction = new URIAction("Share", 
                mobileUri, new URIAction.AltUri(desktopUri));
            shareButton = Button.builder()
                                .action(shareUriAction)
                                .style(Button.ButtonStyle.PRIMARY)
                                .color("#182D36")
                                .gravity(FlexGravity.CENTER)
                                .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        PostbackAction deleteButtonPostback = PostbackAction.builder()
                .label("Delete")
                .data(JsonCreator.createPostback("DELETENOTES", data).toString())
                .build();

        Box placeholderButton = Box
                .builder()
                .layout(FlexLayout.HORIZONTAL)
                .spacing(FlexMarginSize.SM)
                .margin(FlexMarginSize.LG)
                .contents(asList(
                        shareButton,
                        Button.builder()
                                .style(Button.ButtonStyle.SECONDARY)
                                .gravity(FlexGravity.CENTER)
                                .action(deleteButtonPostback)
                                .build()
                ))
                .build();

        Box detailsNotesItemBox = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(asList(placeholderContent, placeholderButton))
                .spacing(FlexMarginSize.SM)
                .margin(FlexMarginSize.MD)
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(detailsNotesItemBox)
                .build();
    }
}
