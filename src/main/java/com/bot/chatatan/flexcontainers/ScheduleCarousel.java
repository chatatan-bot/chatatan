package com.bot.chatatan.flexcontainers;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import org.json.JSONObject;

public class ScheduleCarousel implements Supplier<FlexMessage> {

    private List<JSONObject> scheduleList;
    private LocalDate currentDate;
    private int dayNumber;

    public ScheduleCarousel(List<JSONObject> scheduleList, String detail) {
        this.scheduleList = scheduleList;
        if (detail.equals("next")) {
            this.currentDate = LocalDate.now().plusWeeks(1);
        } else {
            this.currentDate = LocalDate.now();
        }
        this.dayNumber = currentDate.getDayOfWeek().getValue();
    }

    @Override
    public FlexMessage get() {
        List<Bubble> dayBubbles = new ArrayList<>();
        List<JSONObject> events = new ArrayList<>();
        int lastIndex = scheduleList.size() - 1;

        String day = scheduleList.get(0).getString("dayName");
        int diff = DayOfWeek.valueOf(day).getValue() - dayNumber;
        String date = currentDate.plusDays(diff).toString();

        for (int i = 0; i < scheduleList.size(); i++) {
            JSONObject event = scheduleList.get(i);
            events.add(event);

            if (!event.getString("dayName").equals(day)) {
                events.remove(event);
                dayBubbles.add(new DayBubble(day, date, events).get());
                day = event.getString("dayName");
                diff = DayOfWeek.valueOf(day).getValue() - dayNumber;
                date = currentDate.plusDays(diff).toString();
                events.clear();
                events.add(event);
            }
            if (i == lastIndex) {
                dayBubbles.add(new DayBubble(day, date, events).get());
            }
        }

        Carousel carousel = Carousel.builder()
                                    .contents(dayBubbles)
                                    .build();
        return new FlexMessage("Schedule", carousel);
    }

}
