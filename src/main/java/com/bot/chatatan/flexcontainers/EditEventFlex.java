package com.bot.chatatan.flexcontainers;

import static java.util.Arrays.asList;

import com.bot.chatatan.utilities.JsonCreator;
import com.linecorp.bot.model.action.DatetimePickerAction;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import org.json.JSONObject;

public class EditEventFlex implements Supplier<FlexMessage> {

    private JSONObject event;
    private String name;
    private String startTime;
    private String endTime;
    private String date;

    public EditEventFlex(JSONObject event) {
        this.event = event;
        this.name = event.getString("name");
        this.startTime = event.getString("startTime");
        this.endTime = event.getString("endTime");
        this.date = event.getString("date");
    }

    @Override
    public FlexMessage get() {
        final Box headerSection = createHeaderSection(); 
        final Box bodySection = createBodySection(); 
        final Box footerSection = createFooterSection();
        final Bubble bubble = Bubble.builder()
                                .header(headerSection)
                                .body(bodySection)
                                .footer(footerSection)
                                .build();
        return new FlexMessage("Edit event: " + name, bubble);
    }

    private Box createHeaderSection() {
        List<FlexComponent> header = new ArrayList<>();

        header.add(Text.builder()
                    .text(name)
                    .color("#DBB9CA")
                    .size(FlexFontSize.Md)
                    .align(FlexAlign.CENTER)
                    .weight(TextWeight.BOLD)
                    .wrap(true)
                    .build());

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .backgroundColor("#182D36")
                .contents(header)
                .build();
    }

    private Box createBodySection() {
        List<FlexComponent> body = new ArrayList<>();
        Button startTimeButton = timeButton("Start time", "EDITSTARTTIME");
        Button endTimeButton = timeButton("End time", "EDITENDTIME");

        body.add(descriptionText("Event details:"));
        body.add(detailBox());
        body.add(Separator.builder()
                        .color("#182D36")
                        .margin(FlexMarginSize.MD)
                        .build());

        body.add(descriptionText("What do you wish to edit?"));
        body.add(horizontalLayout(startTimeButton, endTimeButton));
        body.add(horizontalLayout(dateButton(), deleteButton()));
        body.add(disclaimerText());

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(body)
                .build();
    }

    private Text descriptionText(String text) {
        return Text.builder()
                .text(text)
                .color("#a3728b")
                .size(FlexFontSize.SM)
                .weight(TextWeight.BOLD)
                .align(FlexAlign.START)
                .margin(FlexMarginSize.LG)
                .wrap(true)
                .build();
    }

    private Box detailBox() {
        String day = event.getString("dayName");
        String isRoutine = event.get("isRoutine").equals("true") ? "Yes" : "No";
        String time = String.format("%s - %s", startTime, endTime);
        
        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(asList(
                        textBox("Day", day),
                        textBox("Date", date),
                        textBox("Time", time),
                        textBox("Routine", isRoutine)
                ))
                .build();
    }

    private Box textBox(String title, String value) {
        return Box.builder()
                .layout(FlexLayout.BASELINE)
                .spacing(FlexMarginSize.SM)
                .contents(asList(
                        Text.builder()
                                .text(title)
                                .color("#000000")
                                .size(FlexFontSize.SM)
                                .flex(1)
                                .build(),
                        Text.builder()
                                .text(value)
                                .color("#000000")
                                .size(FlexFontSize.SM)
                                .flex(1)
                                .build()))
                .build();
    }

    private Box horizontalLayout(Button button1, Button button2) {
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .margin(FlexMarginSize.MD)
                .spacing(FlexMarginSize.SM)
                .contents(asList(
                    button1,
                    button2
                ))
                .build();
    }
    
    private Button timeButton(String label, String command) {
        DatetimePickerAction.OfLocalTime action;
        if (label.equalsIgnoreCase("Start time")) {
            action = DatetimePickerAction.OfLocalTime.builder()
                    .label(label)
                    .initial(LocalTime.parse(startTime))
                    .max(LocalTime.parse(endTime))
                    .data(JsonCreator.createPostback(command, event).toString())
                    .build();
        } else {
            action = DatetimePickerAction.OfLocalTime.builder()
                    .label(label)
                    .initial(LocalTime.parse(endTime))
                    .min(LocalTime.parse(startTime))
                    .data(JsonCreator.createPostback(command, event).toString())
                    .build();
        }

        return Button.builder()
                .action(action)
                .style(Button.ButtonStyle.SECONDARY)
                .gravity(FlexGravity.CENTER)
                .color("#DBB9CA")
                .build();
    }

    private Button dateButton() {
        return Button.builder()
                .action(DatetimePickerAction.OfLocalDate.builder()
                        .label("Date")
                        .initial(LocalDate.now())
                        .min(LocalDate.now())
                        .data(JsonCreator.createPostback("EDITDATE", event).toString())
                        .build())
                .style(Button.ButtonStyle.SECONDARY)
                .gravity(FlexGravity.CENTER)
                .color("#DBB9CA")
                .build();
    }
    
    private Button deleteButton() {
        return Button.builder()
                .action(PostbackAction.builder()
                        .label("Delete")
                        .data(JsonCreator.createPostback("DELETESCHEDULE", event).toString())
                        .build())
                .style(Button.ButtonStyle.SECONDARY)
                .gravity(FlexGravity.CENTER)
                .build();
    }

    private Text disclaimerText() {
        String output = "Unfortunately, we do not support changing the name or routine attribute.";
        output += " If you wish to do that, you can delete this event and make a new one instead.";
        output += "\nThank you!^^";
        return Text.builder()
                .text(output)
                .color("#182D36")
                .size(FlexFontSize.SM)
                .align(FlexAlign.START)
                .margin(FlexMarginSize.MD)
                .wrap(true)
                .build();
    }

    private Box createFooterSection() {
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(asList(
                    Button.builder()
                    .action(PostbackAction.builder()
                            .label("Back")
                            .data(JsonCreator.createButton("HELPSCHEDULE").toString())
                            .build())
                    .flex(0)
                    .gravity(FlexGravity.CENTER)
                    .build())
                ).build();
    }

}