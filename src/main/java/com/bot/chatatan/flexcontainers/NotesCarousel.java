package com.bot.chatatan.flexcontainers;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import org.json.JSONObject;

public class NotesCarousel implements Supplier<FlexMessage> {
    
    private List<JSONObject> notesList;

    public NotesCarousel(List<JSONObject> notesList) {
        this.notesList = notesList;
    }

    @Override
    public FlexMessage get() {
        List<JSONObject> notes = new ArrayList<>();
        List<Bubble> notesBubbles = new ArrayList<>();

        for (int i = 1; i < notesList.size() + 1; i++) {
            if (i % 11 == 0) {
                notesBubbles.add(new NotesBubble(notes).get());
                notes.clear();
                JSONObject note = notesList.get(i - 1);
                notes.add(note);
            } else {
                JSONObject note = notesList.get(i - 1);
                notes.add(note);
            }
        }
        notesBubbles.add(new NotesBubble(notes).get());

        Carousel carousel = Carousel.builder()
                .contents(notesBubbles)
                .build();
        return new FlexMessage("Notes", carousel);
    }
}
