package com.bot.chatatan.flexcontainers;

import static java.util.Arrays.asList;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class FollowFlex implements Supplier<FlexMessage> {

    @Override
    public FlexMessage get() {
        Carousel carousel = Carousel.builder()
                                    .contents(asList(
                                        welcomeBubble(),
                                        featureBubble()
                                    )).build();
        return new FlexMessage("Welcome to Chatatan!", carousel);
    }

    private Bubble welcomeBubble() {
        String logoUrl = "https://drive.google.com/uc?export=view&id=17oF7G7uGh9g4xoEKrJuhlBArg_paRAPs";
        Image heroSection = Image.builder()
                                .url(URI.create(logoUrl))
                                .align(FlexAlign.CENTER)
                                .gravity(FlexGravity.CENTER)
                                .size(Image.ImageSize.FULL_WIDTH)
                                .aspectRatio(Image.ImageAspectRatio.R4TO3)
                                .aspectMode(Image.ImageAspectMode.Fit)
                                .build();

        Box headerSection = helloHeader();
        Box bodySection = Box.builder()
                            .layout(FlexLayout.VERTICAL)
                            .contents(asList(descriptionText()))
                            .build();   

        Bubble bubble = Bubble.builder()
                            .header(headerSection)
                            .hero(heroSection)
                            .body(bodySection)
                            .build();
        return bubble;
    }

    private Bubble featureBubble() {
        String featureImageUrl = "https://drive.google.com/uc?export=view&id=1fTfX8Fk8n7vIWaV-MARa7sdASfwGBMJM";
        Image heroSection = Image.builder()
                                .url(URI.create(featureImageUrl))
                                .align(FlexAlign.CENTER)
                                .gravity(FlexGravity.CENTER)
                                .size(Image.ImageSize.FULL_WIDTH)
                                .aspectRatio(Image.ImageAspectRatio.R20TO13)
                                .aspectMode(Image.ImageAspectMode.Cover)
                                .build();

        Box headerSection = featureHeader();
        Box bodySection = Box.builder()
                            .layout(FlexLayout.VERTICAL)
                            .spacing(FlexMarginSize.MD)
                            .contents(asList(
                                featureText(),
                                new FeatureButtons().get()
                            ))
                            .build();

        Bubble bubble = Bubble.builder()
                            .header(headerSection)
                            .hero(heroSection)
                            .body(bodySection)
                            .build();
        return bubble;
    }

    private Box helloHeader() {
        List<FlexComponent> header = new ArrayList<>();

        header.add(Text.builder()
                    .text("Hello,")
                    .color("#182D36")
                    .size(FlexFontSize.XL)
                    .align(FlexAlign.START)
                    .weight(TextWeight.BOLD)
                    .build());
        
        header.add(Text.builder()
                    .text("Chatatan's here.")
                    .color("#a3728b")
                    .size(FlexFontSize.LG)
                    .align(FlexAlign.START)
                    .weight(TextWeight.BOLD)
                    .build());

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(header)
                .build();
    }

    private Box featureHeader() {
        List<FlexComponent> header = new ArrayList<>();
        
        header.add(Text.builder()
                    .text("We present you two features:")
                    .color("#a3728b")
                    .size(FlexFontSize.Md)
                    .align(FlexAlign.END)
                    .weight(TextWeight.BOLD)
                    .build());
        
        header.add(Text.builder()
                    .text("Notes and Schedule")
                    .color("#182D36")
                    .size(FlexFontSize.Md)
                    .align(FlexAlign.END)
                    .weight(TextWeight.BOLD)
                    .build());

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(header)
                .build();
    }

    private Text descriptionText() {
        return Text.builder()
                    .text("I will serve as your personal notes and schedule keeper!^^")
                    .color("#182D36")
                    .size(FlexFontSize.Md)
                    .align(FlexAlign.START)
                    .wrap(true)
                    .build();
    }

    private Text featureText() {
        return Text.builder()
                    .text("So, where shall we start?")
                    .color("#a3728b")
                    .size(FlexFontSize.Md)
                    .align(FlexAlign.START)
                    .weight(TextWeight.BOLD)
                    .build();
    }

}
