package com.bot.chatatan.flexcontainers;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class FeatureFlex implements Supplier<FlexMessage> {

    @Override
    public FlexMessage get() {
        Box headerSection = createHeaderSection(); 
        Box bodySection = new FeatureButtons().get();
        Bubble bubble = Bubble.builder()
                .header(headerSection)
                .body(bodySection)
                .build();
        return new FlexMessage("Select feature", bubble);
    }

    private Box createHeaderSection() {
        List<FlexComponent> header = new ArrayList<>();

        header.add(title("Hello, Chatatan's here."));
        header.add(title("How can I help you today?"));

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .backgroundColor("#182D36")
                .contents(header)
                .build();
    }

    private Text title(String text) {
        return Text.builder()
                    .text(text)
                    .color("#DBB9CA")
                    .size(FlexFontSize.Md)
                    .align(FlexAlign.CENTER)
                    .weight(TextWeight.BOLD)
                    .build();
    }
    
}
