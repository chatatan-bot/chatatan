package com.bot.chatatan.flexcontainers;

import static java.util.Arrays.asList;

import com.linecorp.bot.model.action.URIAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class ShareScheduleFlex implements Supplier<FlexMessage> {

    private String schedule;

    public ShareScheduleFlex(String schedule) {
        this.schedule = schedule;
    }

    @Override
    public FlexMessage get() {
        final Box headerSection = createHeaderSection(); 
        final Box bodySection = createBodySection(); 
        final Box bottomSection = createBottomSection();
        final Bubble bubble = Bubble.builder()
                                    .header(headerSection)
                                    .body(bodySection)
                                    .footer(bottomSection)
                                    .build();
        return new FlexMessage("Share Schedule", bubble);
    }

    private Box createHeaderSection() {
        List<FlexComponent> header = new ArrayList<>();

        header.add(Text.builder()
                    .text("Schedule Details")
                    .color("#DBB9CA")
                    .size(FlexFontSize.LG)
                    .align(FlexAlign.CENTER)
                    .weight(TextWeight.BOLD)
                    .build());

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .backgroundColor("#182D36")
                .contents(header)
                .build();
    }

    private Box createBodySection() {
        List<FlexComponent> body = new ArrayList<>();
    
        String[] values = schedule.split("\n");
        for (int i = 0; i < values.length - 1; i++) {
            if (values[i].endsWith("DAY")) {
                body.add(Text.builder()
                            .text(values[i]) 
                            .color("#a3728b")
                            .size(FlexFontSize.Md)
                            .margin(FlexMarginSize.LG)
                            .align(FlexAlign.START)
                            .weight(TextWeight.BOLD)
                            .wrap(true)
                            .build()
                );
                body.add(Separator.builder()
                    .color("#182D36")
                    .build()
                );
            } else {
                Text eventTime = Text.builder()
                                .text(values[i]) 
                                .color("#182D36")
                                .size(FlexFontSize.SM)
                                .align(FlexAlign.END)
                                .wrap(true)
                                .build();
                i++;
                Text eventName = Text.builder()
                                .text(values[i]) 
                                .color("#182D36")
                                .size(FlexFontSize.SM)
                                .align(FlexAlign.START)
                                .wrap(true)
                                .build();
                Box eventBox = Box.builder()
                                .layout(FlexLayout.HORIZONTAL)
                                .spacing(FlexMarginSize.SM)
                                .contents(asList(
                                    eventName,
                                    eventTime
                                ))
                                .build();
                body.add(eventBox);
            }
        }

        return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .spacing(FlexMarginSize.SM)
            .contents(body)
            .build();
                
    }

    private Box createBottomSection() {
        List<FlexComponent> bottom = new ArrayList<>();
        try {
            URI mobileUri = new URI("line://msg/text" 
                + URLEncoder.encode(schedule, "UTF-8").replace("+", "%20"));
            URI desktopUri = new URI("https://www.google.com/");
            URIAction shareUriAction = new URIAction("Share", 
                mobileUri, new URIAction.AltUri(desktopUri));
            bottom.add(
                Button.builder()
                    .action(shareUriAction)
                    .style(Button.ButtonStyle.PRIMARY)
                    .color("#182D36")
                    .gravity(FlexGravity.CENTER) 
                    .flex(0)
                    .build()
            );
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .spacing(FlexMarginSize.SM)
            .contents(bottom)
            .build();
    }

}