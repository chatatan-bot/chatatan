package com.bot.chatatan.flexcontainers;

import static java.util.Arrays.asList;

import com.bot.chatatan.utilities.JsonCreator;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import org.json.JSONObject;

public class DayBubble implements Supplier<Bubble> {

    private final String editCommand = "EDITSCHEDULE";
    private final String deleteCommand = "DELETESCHEDULE";
    private List<JSONObject> events;
    private String day;
    private String date;

    public DayBubble(String day, String date, List<JSONObject> events) {
        this.day = day;
        this.date = date;
        this.events = events;
    }

    @Override
    public Bubble get() {
        Box dayName = createHeaderSection(day, date); 
        Box dayBody = createBodyBlock(events);
        return Bubble.builder()
                    .header(dayName)
                    .body(dayBody)
                    .build();
    }

    private Box createHeaderSection(String day, String date) {
        List<FlexComponent> header = new ArrayList<>();

        header.add(Text.builder()
                    .text(day)
                    .color("#DBB9CA")
                    .size(FlexFontSize.LG)
                    .align(FlexAlign.CENTER)
                    .weight(TextWeight.BOLD)
                    .build());
        
        header.add(Text.builder()
                    .text(date)
                    .color("#DBB9CA")
                    .size(FlexFontSize.Md)
                    .align(FlexAlign.CENTER)
                    .weight(TextWeight.BOLD)
                    .build());

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .backgroundColor("#182D36")
                .contents(header)
                .build();
    }

    private Box createBodyBlock(List<JSONObject> events) {
        List<FlexComponent> scheduleListBox = new ArrayList<>();

        for (JSONObject event: events) {
            String startTime = event.getString("startTime");
            String endTime = event.getString("endTime");
            String time = String.format("%s - %s", startTime, endTime);

            Box placeholderContent = Box
                    .builder()
                    .layout(FlexLayout.VERTICAL)
                    .spacing(FlexMarginSize.SM)
                    .contents(asList(
                            Text.builder()
                                .text(event.getString("name"))
                                .color("#182D36")
                                .size(FlexFontSize.Md)
                                .weight(TextWeight.BOLD)
                                .flex(1)
                                .wrap(true)
                                .build(),
                            textBox("Time", time)
                    ))
                    .build();

            PostbackAction editButtonPostback = PostbackAction.builder()
                    .label("Edit")
                    .data(JsonCreator.createPostback(editCommand, event).toString())
                    .build();

            PostbackAction deleteButtonPostback = PostbackAction.builder()
                    .label("Delete")
                    .data(JsonCreator.createPostback(deleteCommand, event).toString())
                    .build();

            Box placeholderButton = Box
                    .builder()
                    .layout(FlexLayout.HORIZONTAL)
                    .spacing(FlexMarginSize.SM)
                    .contents(asList(
                            Button.builder()
                                    .style(Button.ButtonStyle.SECONDARY)
                                    .color("#DBB9CA")
                                    .gravity(FlexGravity.CENTER)
                                    .action(editButtonPostback)
                                    .build(),
                            Button.builder()
                                    .style(Button.ButtonStyle.SECONDARY)
                                    .gravity(FlexGravity.CENTER)
                                    .action(deleteButtonPostback)
                                    .build()
                    ))
                    .build();

            Box scheduleItemBox = Box.builder()
                    .layout(FlexLayout.VERTICAL)
                    .contents(asList(placeholderContent, placeholderButton))
                    .spacing(FlexMarginSize.SM)
                    .margin(FlexMarginSize.MD)
                    .build();

            scheduleListBox.add(scheduleItemBox);
        }

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(scheduleListBox)
                .build();
    }

    private Box textBox(String title, String value) {
        return Box.builder()
                .layout(FlexLayout.BASELINE)
                .spacing(FlexMarginSize.SM)
                .contents(asList(
                        Text.builder()
                                .text(title)
                                .color("#000000")
                                .size(FlexFontSize.SM)
                                .flex(1)
                                .build(),
                        Text.builder()
                                .text(value)
                                .color("#000000")
                                .size(FlexFontSize.SM)
                                .flex(1)
                                .build()))
                .build();
    }
}