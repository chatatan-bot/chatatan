package com.bot.chatatan.flexcontainers;

import static java.util.Arrays.asList;

import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import org.json.JSONObject;

public class NotesBubble implements Supplier<Bubble> {

    private final String detailCommand = "DETAILSNOTES";
    private List<JSONObject> notes;

    public NotesBubble(List<JSONObject> notes) {
        this.notes = notes;
    }

    @Override
    public Bubble get() {
        Box title = createHeaderSection();
        Box body = createBodyBlock();
        return Bubble.builder()
                .header(title)
                .body(body)
                .build();
    }

    private Box createHeaderSection() {
        Text headerText = Text.builder()
                .text("Your Notes")
                .color("#DBB9CA")
                .size(FlexFontSize.LG)
                .align(FlexAlign.CENTER)
                .weight(TextWeight.BOLD)
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .backgroundColor("#182D36")
                .contents(headerText)
                .build();
    }

    private Box createBodyBlock() {
        List<FlexComponent> notesListBox = new ArrayList<>();

        for (JSONObject note: notes) {
            note.put("command", detailCommand);

            PostbackAction titlePostbackAction = PostbackAction.builder()
                    .label(note.getString("title"))
                    .data(note.toString())
                    .build();

            Box placeholderButton = Box
                    .builder()
                    .layout(FlexLayout.HORIZONTAL)
                    .spacing(FlexMarginSize.SM)
                    .contents(asList(
                            Button.builder()
                                    .style(Button.ButtonStyle.LINK)
                                    .gravity(FlexGravity.CENTER)
                                    .action(titlePostbackAction)
                                    .build()
                    ))
                    .build();

            Box noteItemBox = Box.builder()
                    .layout(FlexLayout.VERTICAL)
                    .contents(asList(placeholderButton))
                    .spacing(FlexMarginSize.SM)
                    .margin(FlexMarginSize.MD)
                    .build();

            notesListBox.add(noteItemBox);
        }

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(notesListBox)
                .build();
    }
}
