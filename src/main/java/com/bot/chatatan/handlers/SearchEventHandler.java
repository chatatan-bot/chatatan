package com.bot.chatatan.handlers;

import com.bot.chatatan.flexcontainers.SearchOutputFlex;
import com.bot.chatatan.strategy.Feature;
import com.linecorp.bot.model.event.postback.PostbackContent;
import com.linecorp.bot.model.message.TextMessage;

import java.util.List;
import org.json.JSONObject;

public class SearchEventHandler extends BaseHandler {

    @Override
    public boolean handle(String userId, String command) throws Exception {
        if (command.equals("SEARCH")) {
            PostbackContent content = HandlerManager.getContent();
            Feature feature = HandlerManager.getFeature();

            String date = content.getParams().get("date");
            List<JSONObject> searchedEvents = feature.search(date, userId);

            if (searchedEvents.size() == 0) {
                message = new TextMessage("No events found..");
            } else {
                message = new SearchOutputFlex(searchedEvents, date).get();
            }
            return true;
        }
        return false;
    }

}
