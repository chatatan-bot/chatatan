package com.bot.chatatan.handlers;

import com.linecorp.bot.model.message.Message;

public abstract class BaseHandler {

    protected Message message;

    public abstract boolean handle(String userId, String inputMessage) throws Exception;

    public Message getMessage() {
        return this.message;
    }
    
}