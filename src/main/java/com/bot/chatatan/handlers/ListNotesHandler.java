package com.bot.chatatan.handlers;

import com.bot.chatatan.flexcontainers.NotesCarousel;
import com.bot.chatatan.strategy.Feature;
import com.linecorp.bot.model.message.TextMessage;

import java.util.List;
import org.json.JSONObject;

public class ListNotesHandler extends BaseHandler {

    @Override
    public boolean handle(String userId, String command) throws Exception {
        if (command.equals("LISTNOTES")) {
            Feature feature = HandlerManager.getFeature();
            feature.setListBehaviour("listNotes");

            List<JSONObject> notes = feature.list(userId);
            if (notes.size() == 0) {
                message = new TextMessage("No notes found..");
            } else {
                message = new NotesCarousel(notes).get();
            }
            return true;
        }
        return false;
    }
}
