package com.bot.chatatan.handlers;

import com.bot.chatatan.flexcontainers.ScheduleGuide;

public class HelpScheduleHandler extends BaseHandler {

    @Override
    public boolean handle(String userId, String command) {
        if (command.equals("HELPSCHEDULE")) {
            message = new ScheduleGuide().get();
            return true;
        }
        return false;
    }

}
    