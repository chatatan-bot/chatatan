package com.bot.chatatan.handlers;

import com.bot.chatatan.flexcontainers.EditEventFlex;

import org.json.JSONObject;

public class EditScheduleHandler extends BaseHandler {

    @Override
    public boolean handle(String userId, String command) {
        if (command.equals("EDITSCHEDULE")) {
            JSONObject data = HandlerManager.getData();
            message = new EditEventFlex(data).get();
            return true;
        }
        return false;
    }

}
