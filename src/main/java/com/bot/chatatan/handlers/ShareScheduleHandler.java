package com.bot.chatatan.handlers;

import com.bot.chatatan.flexcontainers.ShareScheduleFlex;
import com.bot.chatatan.strategy.Feature;
import com.linecorp.bot.model.message.TextMessage;

import org.json.JSONObject;

public class ShareScheduleHandler extends BaseHandler {
    
    @Override
    public boolean handle(String userId, String command) throws Exception {
        if (command.equals("SHARESCHEDULE")) {
            JSONObject data = HandlerManager.getData();
            Feature feature = HandlerManager.getFeature();
            feature.setShareBehaviour("shareSchedule");

            feature.updateSchedule(userId);
            String detail = data.getString("detail");
            String extendedUrl = String.format("%s/%s", detail, userId);
            String schedule = feature.share(extendedUrl);

            if (schedule == null) {
                message = new TextMessage("No events found..");
            } else {
                message = new ShareScheduleFlex(schedule).get();
            }
            return true;
        }
        return false;
    }
}
