package com.bot.chatatan.handlers;

import com.bot.chatatan.flexcontainers.FeatureFlex;
import com.bot.chatatan.strategy.Feature;
import com.linecorp.bot.model.event.postback.PostbackContent;
import com.linecorp.bot.model.message.Message;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class HandlerManager {

    private List<BaseHandler> textMessageHandlers;
    private List<BaseHandler> postbackHandlers;
    private Message unknownQueryMessage;

    private static Feature feature;
    private static String[] inputValues;
    private static JSONObject data;
    private static PostbackContent content;

    public HandlerManager() {
        this.textMessageHandlers = new ArrayList<>();
        this.postbackHandlers = new ArrayList<>();

        this.unknownQueryMessage = new FeatureFlex().get();
        initialize();
    }

    private void initialize() {
        textMessageHandlers.add(new AddTextMessageHandler());
        textMessageHandlers.add(new WriteTextMessageHandler());

        postbackHandlers.add(new HelpNotesHandler());
        postbackHandlers.add(new HelpScheduleHandler());
        postbackHandlers.add(new ListScheduleHandler());
        postbackHandlers.add(new DeleteEventHandler());
        postbackHandlers.add(new EditScheduleHandler());
        postbackHandlers.add(new EditEventHandler());
        postbackHandlers.add(new SearchEventHandler());
        postbackHandlers.add(new ListNotesHandler());
        postbackHandlers.add(new DetailNotesHandler());
        postbackHandlers.add(new DeleteNotesHandler());
        postbackHandlers.add(new ShareScheduleHandler());
    }

    public Message handleUnknownQueries() {
        return this.unknownQueryMessage;
    }

    public Message handleFollowEvent(String userId) throws Exception {
        BaseHandler followHandler = new FollowHandler();
        followHandler.handle(userId, "");
        return followHandler.getMessage();
    }

    public List<BaseHandler> getTextMessageHandlers() {
        return this.textMessageHandlers;
    }

    public List<BaseHandler> getPostbackHandlers() {
        return this.postbackHandlers;
    }

    public void setData(JSONObject inputData) {
        HandlerManager.data = inputData;
    }

    public static JSONObject getData() {
        return data;
    }

    public void setContent(PostbackContent inputContent) {
        HandlerManager.content = inputContent;
    }

    public static PostbackContent getContent() {
        return content;
    }

    public void setInputValues(String[] values) {
        HandlerManager.inputValues = values;
    }

    public static String[] getInputValues() {
        return inputValues;
    }

    public void setFeature(Feature feature) {
        HandlerManager.feature = feature;
    }

    public static Feature getFeature() {
        return feature;
    }

}