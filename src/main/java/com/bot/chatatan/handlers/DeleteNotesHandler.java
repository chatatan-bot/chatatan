package com.bot.chatatan.handlers;

import com.bot.chatatan.strategy.Feature;
import com.linecorp.bot.model.message.TextMessage;

import org.json.JSONObject;

public class DeleteNotesHandler extends BaseHandler {

    @Override
    public boolean handle(String userId, String command) throws Exception {
        if (command.equals("DELETENOTES")) {
            JSONObject data = HandlerManager.getData();
            Feature feature = HandlerManager.getFeature();
            feature.setDeleteBehaviour("deleteNotes");

            String deleteResponse = feature.delete(data);
            message = new TextMessage(deleteResponse);
            return true;
        }
        return false;
    }

}