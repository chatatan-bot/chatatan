package com.bot.chatatan.handlers;

import com.bot.chatatan.flexcontainers.FollowFlex;
import com.bot.chatatan.strategy.Feature;

public class FollowHandler extends BaseHandler {

    @Override
    public boolean handle(String userId, String command) {
        Feature feature = HandlerManager.getFeature();
        feature.follow(userId);
        message = new FollowFlex().get();
        return true;
    }

}