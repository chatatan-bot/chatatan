package com.bot.chatatan.handlers;

import com.bot.chatatan.flexcontainers.NotesDetailsBubble;
import com.bot.chatatan.strategy.Feature;

import org.json.JSONObject;

public class DetailNotesHandler extends BaseHandler {

    @Override
    public boolean handle(String userId, String command) throws Exception {
        if (command.equals("DETAILSNOTES")) {
            JSONObject data = HandlerManager.getData();
            Feature feature = HandlerManager.getFeature();
            feature.setShareBehaviour("shareNotes");

            String retrieveDetails = feature.share(data.getString("id"));
            message = new NotesDetailsBubble(retrieveDetails, data).get();
            return true;
        }
        return false;
    }

}