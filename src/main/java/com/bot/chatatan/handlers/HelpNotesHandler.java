package com.bot.chatatan.handlers;

import com.bot.chatatan.flexcontainers.NotesGuide;

public class HelpNotesHandler extends BaseHandler {

    @Override
    public boolean handle(String userId, String command) {
        if (command.equals("HELPNOTES")) {
            message = new NotesGuide().get();
            return true;
        }
        return false;
    }

}
