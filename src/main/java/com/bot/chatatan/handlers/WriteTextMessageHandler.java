package com.bot.chatatan.handlers;

import com.bot.chatatan.strategy.Feature;
import com.linecorp.bot.model.message.TextMessage;

public class WriteTextMessageHandler extends BaseHandler {

    @Override
    public boolean handle(String userId, String command) throws Exception {
        if (command.equals("WRITE")) {
            String[] inputValues = HandlerManager.getInputValues();
            if (inputValues.length < 3) {
                message = new TextMessage("The message you send is incomplete :(");
                return true;
            }
            
            Feature feature = HandlerManager.getFeature();
            feature.setAddBehaviour("addToNotes");
            feature.add(userId, inputValues);
            message = new TextMessage("Noted! Your notes has been saved :)");
            return true;
        }
        return false;
    }

}
    