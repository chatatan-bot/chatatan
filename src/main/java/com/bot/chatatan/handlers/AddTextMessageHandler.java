package com.bot.chatatan.handlers;

import com.bot.chatatan.strategy.Feature;
import com.linecorp.bot.model.message.TextMessage;
import java.util.Arrays;

public class AddTextMessageHandler extends BaseHandler {

    @Override
    public boolean handle(String userId, String command) throws Exception {
        if (command.equals("ADD")) {
            handleAddException(userId);
            return true;
        }
        return false;
    }

    private void handleAddException(String userId) throws Exception {
        try {
            String[] inputValues = HandlerManager.getInputValues();
            Feature feature = HandlerManager.getFeature();
            feature.setAddBehaviour("addToEvents");

            String[] values = Arrays.copyOfRange(inputValues, 1, inputValues.length);
            feature.add(userId, values);
            message = new TextMessage("Noted! Your event has been saved :)");
        } catch (IllegalArgumentException e) {
            String response = e.getMessage() + "\n\nPlease resend it with the correct format.";
            message = new TextMessage(response);
        } catch (IndexOutOfBoundsException e) {
            message = new TextMessage("The message you send is incomplete :(");
        } 
    }

}
    