package com.bot.chatatan.handlers;

import com.bot.chatatan.flexcontainers.ScheduleCarousel;
import com.bot.chatatan.strategy.Feature;
import com.linecorp.bot.model.message.TextMessage;

import java.util.List;
import org.json.JSONObject;

public class ListScheduleHandler extends BaseHandler {

    @Override
    public boolean handle(String userId, String command) throws Exception {
        if (command.equals("LISTSCHEDULE")) {
            JSONObject data = HandlerManager.getData();
            Feature feature = HandlerManager.getFeature();
            feature.setListBehaviour("listSchedule");

            feature.updateSchedule(userId);
            String detail = data.getString("detail");
            String extendedUrl = String.format("%s/%s", detail, userId);
            List<JSONObject> events = feature.list(extendedUrl);

            if (events.size() == 0) {
                message = new TextMessage("No events found..");
            } else {
                message = new ScheduleCarousel(events, detail).get();
            }
            return true;
        }
        return false;
    }

}
