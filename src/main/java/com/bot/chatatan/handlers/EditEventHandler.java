package com.bot.chatatan.handlers;

import com.bot.chatatan.flexcontainers.EditEventFlex;
import com.bot.chatatan.strategy.Feature;
import com.bot.chatatan.utilities.JsonCreator;
import com.linecorp.bot.model.event.postback.PostbackContent;
import com.linecorp.bot.model.message.TextMessage;

import java.util.List;
import org.json.JSONObject;

public class EditEventHandler extends BaseHandler {

    @Override
    public boolean handle(String userId, String command) throws Exception {
        switch (command) {
            case ("EDITSTARTTIME"):
                return edit("startTime");
            case ("EDITENDTIME"):
                return edit("endTime");
            case ("EDITDATE"):
                return edit("date");
            default:
                return false;
        }
    }

    private boolean edit(String attribute) throws Exception {
        JSONObject data = HandlerManager.getData();
        String value = getParam(attribute);
        data.put(attribute, value);

        Feature feature = HandlerManager.getFeature();
        List<Object> eventObjects = feature.edit(data);
        if (eventObjects.size() == 0) {
            message = new TextMessage("Sorry, the event you wish to edit doesn't exist :(");
        } else {  
            JSONObject editedEvent = JsonCreator.convertObject(eventObjects.get(0));
            message = new EditEventFlex(editedEvent).get();
        }
        return true;
    }

    private String getParam(String attribute) {
        PostbackContent content = HandlerManager.getContent();
 
        if (attribute.endsWith("Time")) {
            return content.getParams().get("time");
        } else {
            return content.getParams().get("date");
        }
    }

}
    