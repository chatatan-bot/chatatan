package com.bot.chatatan.utilities;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ApiFetcher {

    @Autowired
    RestTemplate restTemplate;

    String baseUrl;
    final String notesBaseUrl = "http://notesapi/";
    final String scheduleBaseUrl = "http://scheduleapi/";

    public void setBaseUrl(String feature) {
        this.baseUrl = feature.equalsIgnoreCase("schedule") ? scheduleBaseUrl : notesBaseUrl;
    }

    public List<JSONObject> getList(String extendedUrl) {
        String url = String.format("%s/%s", baseUrl, extendedUrl);
        List<Object> result = restTemplate.getForObject(url, List.class);

        List<JSONObject> jsonList = new ArrayList<>();
        for (Object jsonData: result) {
            JSONObject json = JsonCreator.convertObject(jsonData);
            jsonList.add(json);
        }
        return jsonList;
    }

    public String getString(String extendedUrl) {
        String url = String.format("%s/%s", baseUrl, extendedUrl);
        String result = restTemplate.getForObject(url, String.class);
        return result;
    }

    public ResponseEntity<Void> post(String extendedUrl, JSONObject data) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(data.toString(), headers);
        
        String url = String.format("%s/%s", baseUrl, extendedUrl);
        ResponseEntity<Void> response = restTemplate.postForEntity(url, request, Void.class);
        return response;
    }

    public ResponseEntity<Void> post(String extendedUrl) {
        String url = String.format("%s/%s", scheduleBaseUrl, extendedUrl);
        ResponseEntity<Void> response = restTemplate.postForEntity(url, null, Void.class);
        return response;
    }

    public ResponseEntity<List> put(JSONObject data) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(data.toString(), headers);

        String url = String.format("%s/event", scheduleBaseUrl);
        ResponseEntity<List> response;
        response = restTemplate.exchange(url, HttpMethod.PUT, request, List.class);
        return response;
    }
    
    public ResponseEntity<String> delete(String extendedUrl, JSONObject data) {
        String url = String.format("%s/%s/%s", baseUrl, extendedUrl, data.getLong("id"));
        ResponseEntity<String> response;
        response = restTemplate.exchange(url, HttpMethod.DELETE, null, String.class);
        return response;
    }
}
