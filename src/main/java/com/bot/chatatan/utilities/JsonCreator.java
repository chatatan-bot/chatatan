package com.bot.chatatan.utilities;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Arrays;

import org.json.JSONObject;

public class JsonCreator {

    public static JSONObject createEvent(String userId, String[] values) 
        throws IllegalArgumentException {
            
        try {
            LocalTime startTime = LocalTime.parse(values[1]);
            LocalTime endTime = LocalTime.parse(values[2]);
            if (startTime.compareTo(endTime) >= 0) {
                throw new IllegalArgumentException("Are you sure the time was correct?");
            }
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException("Follow the time format (hh:mm) please :(");
        }

        JSONObject json = new JSONObject();
        json.put("userId", userId);
        json.put("name", values[0]);
        json.put("startTime", values[1]);
        json.put("endTime", values[2]);

        String isRoutine = values[4].toUpperCase();
        if (isRoutine.equals("YES")) {
            json.put("isRoutine", true);
        } else if (isRoutine.equals("NO")) {
            json.put("isRoutine", false);
        } else {
            throw new IllegalArgumentException("So, is it routine or nah?");
        }

        try {
            LocalDate date = LocalDate.parse(values[3]);
            json.put("date", values[3]);
            json.put("dayName", date.getDayOfWeek().toString());
        } catch (Exception e) {
            String dayName = values[3].toUpperCase();
            if (dayName.endsWith("DAY")) {
                json.put("dayName", dayName);
            } else {
                throw new IllegalArgumentException("The date/day was in the wrong format..");
            }
        }
        return json;
    }

    public static JSONObject createNotes(String userId, String[] values) {
        String[] content = Arrays.copyOfRange(values, 2, values.length);
        String notes = String.join("\n", content);

        JSONObject json = new JSONObject();
        json.put("userId", userId);
        json.put("title", values[1]);
        json.put("notes", notes);
        return json;
    }

    public static JSONObject convertObject(Object object) {
        String jsonString = JSONObject.valueToString(object);
        return new JSONObject(jsonString);
    }

    public static JSONObject createButton(String command) { 
        JSONObject json = new JSONObject();  
        json.put("command", command);
        return json;
    }

    public static JSONObject createPostback(String command, JSONObject data) {   
        data.put("command", command);
        data.remove("details");
        return data;
    }

    public static JSONObject createAction(String command, String detail) { 
        String[] input = detail.split(" ");
        JSONObject json = new JSONObject();  
        json.put("command", command);
        json.put("detail", input[0].toLowerCase());
        return json;
    }
}