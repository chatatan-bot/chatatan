package com.bot.chatatan.controller;

import com.bot.chatatan.handlers.BaseHandler;
import com.bot.chatatan.handlers.HandlerManager;
import com.bot.chatatan.strategy.Feature;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.FollowEvent;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.postback.PostbackContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@LineMessageHandler
public class MainController {

    private LineMessagingClient client;
    private HandlerManager handlerManager;

    @Autowired
    public MainController(
            LineMessagingClient lineMessagingClient, 
            @Qualifier("feature") Feature feature) {
        this.client = lineMessagingClient;
        this.handlerManager = new HandlerManager();
        this.handlerManager.setFeature(feature);
    }

    private void reply(String replyToken, Message message) throws Exception {
        client.replyMessage(new ReplyMessage(replyToken, message)).get();
    }

    @EventMapping
    public void handleFollowEvent(FollowEvent event) throws Exception {
        String replyToken = event.getReplyToken();
        String userId = event.getSource().getUserId();
        Message followMessage = handlerManager.handleFollowEvent(userId);
        reply(replyToken, followMessage);
    }

    @EventMapping
    public void handleTextMessageEvent(MessageEvent<TextMessageContent> event) throws Exception {
        String replyToken = event.getReplyToken();
        String userId = event.getSource().getUserId();
        String inputMessage = event.getMessage().getText();
        String[] data = inputMessage.split("\n");
        handlerManager.setInputValues(data);

        String command = data[0].toUpperCase().trim();
        List<BaseHandler> handlers = handlerManager.getTextMessageHandlers();
        Message textMessage = handleEvent(userId, command, handlers);
        reply(replyToken, textMessage);
    }

    @EventMapping
    public void handlePostbackEvent(PostbackEvent event) throws Exception {
        String replyToken = event.getReplyToken();
        String userId = event.getSource().getUserId();
        PostbackContent content = event.getPostbackContent();
        JSONObject json = new JSONObject(content.getData());
        handlerManager.setContent(content);
        handlerManager.setData(json);

        String command = json.remove("command").toString();
        List<BaseHandler> handlers = handlerManager.getPostbackHandlers();
        Message postbackMessage = handleEvent(userId, command, handlers);
        reply(replyToken, postbackMessage);
    }

    private Message handleEvent(String userId, String command, List<BaseHandler> handlers) {
        try {
            for (BaseHandler handler: handlers) {
                if (handler.handle(userId, command)) {
                    return handler.getMessage();
                }
            }
            return handlerManager.handleUnknownQueries();
        } catch (Exception e) {
            String errorMessage = "Sorry, we are having technical issues. Please try again later.";
            return new TextMessage(errorMessage);
        } 
    }
    
}
