package com.bot.chatatan.strategy;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.bot.chatatan.utilities.JsonCreator;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class NotesFeatureTest {

    @Autowired
    @Qualifier("feature")
    Feature feature;

    @Autowired
    @Qualifier("addToNotes")
    AddBehaviour addToNotesBehaviour;

    @Autowired
    @Qualifier("listNotes")
    ListBehaviour listNotesBehaviour;

    @Autowired
    @Qualifier("deleteNotes")
    DeleteBehaviour deleteNotesBehaviour;

    @Autowired
    @Qualifier("shareNotes")
    ShareBehaviour shareNotesBehaviour;

    String userId;

    @Before
    public void setUp() throws Exception {
        userId = "userId";
        feature.setAddBehaviour("addToNotes");
        feature.setDeleteBehaviour("deleteNotes");
        feature.setListBehaviour("listNotes");
        feature.setShareBehaviour("shareNotes");
    }

    @Test
    public void testGetName() {
        assertEquals(addToNotesBehaviour.getName(), "addToNotes");
        assertEquals(deleteNotesBehaviour.getName(), "deleteNotes");
        assertEquals(listNotesBehaviour.getName(), "listNotes");
        assertEquals(shareNotesBehaviour.getName(), "shareNotes");
    }

    @Test
    public void testGetBehaviours() {
        assertTrue(feature.getAddBehaviour() instanceof AddToNotes);
        assertTrue(feature.getListBehaviour() instanceof ListNotes);
        assertTrue(feature.getDeleteBehaviour() instanceof DeleteNotes);
        assertTrue(feature.getShareBehaviour() instanceof ShareNotes);
    }

    @Test
    public void testWriteNotesSuccess() {
        String[] input = { "WRITE", "TDD Notes Feature", "Fetch Notes API\nHope this works" };
        ResponseEntity<Void> response = feature.add(userId, input);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void testListReturnMethodFromListBehaviour() {
        String expectedOutput = listNotesBehaviour.list(userId).toString();
        assertEquals(feature.list(userId).toString(), expectedOutput);
    }

    @Test
    public void testDeleteReturnMethodFromDeleteBehaviour() {
        String[] input = { "WRITE", "TDD Notes Feature", "Fetch Notes API\nHope this works" };
        JSONObject notes = JsonCreator.createNotes(userId, input);
        notes.put("id", 15);
        assertEquals(feature.delete(notes), deleteNotesBehaviour.delete(notes));
    }

    @Test
    public void testShareReturnMethodFromShareBehaviour() {
        assertEquals(feature.share("2"), shareNotesBehaviour.share("2"));
    }
    
}
