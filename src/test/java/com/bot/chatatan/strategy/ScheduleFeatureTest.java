package com.bot.chatatan.strategy;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.bot.chatatan.utilities.JsonCreator;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class ScheduleFeatureTest {

    @Autowired
    @Qualifier("feature")
    Feature feature;

    @Autowired
    @Qualifier("addToEvents")
    AddBehaviour addToEventsBehaviour;

    @Autowired
    @Qualifier("listSchedule")
    ListBehaviour listScheduleBehaviour;

    @Autowired
    @Qualifier("deleteEvent")
    DeleteBehaviour deleteEventBehaviour;

    @Autowired
    @Qualifier("shareSchedule")
    ShareBehaviour shareScheduleBehaviour;

    String userId;

    @Before
    public void setUp() throws Exception {
        userId = "1806205666";
        feature.setAddBehaviour("addToEvents");
        feature.setDeleteBehaviour("deleteEvent");
        feature.setListBehaviour("listSchedule");
        feature.setShareBehaviour("shareSchedule");
    }

    @Test
    public void testGetName() {
        assertEquals(addToEventsBehaviour.getName(), "addToEvents");
        assertEquals(deleteEventBehaviour.getName(), "deleteEvent");
        assertEquals(listScheduleBehaviour.getName(), "listSchedule");
        assertEquals(shareScheduleBehaviour.getName(), "shareSchedule");
    }

    @Test
    public void testGetDefaultBehaviours() {
        assertTrue(feature.getAddBehaviour() instanceof AddToEvents);
        assertTrue(feature.getListBehaviour() instanceof ListSchedule);
        assertTrue(feature.getDeleteBehaviour() instanceof DeleteEvent);
        assertTrue(feature.getShareBehaviour() instanceof ShareSchedule);
    }

    @Test
    public void testAddEventSuccess() {
        String[] input = { "TDD Json Creator", "20:30", "23:30", "2020-04-23", "no" };
        ResponseEntity<Void> response = feature.add(userId, input);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void testListReturnMethodFromListBehaviour() {
        String extendedUrl = "next/" + userId;
        assertEquals(feature.list(extendedUrl), listScheduleBehaviour.list(extendedUrl));
    }

    @Test
    public void testDeleteReturnMethodFromDeleteBehaviour() {
        String[] input = { "TDD Json Creator", "20:30", "23:30", "2020-04-23", "no" };
        JSONObject event = JsonCreator.createEvent(userId, input);
        event.put("id", 1);
        assertEquals(feature.delete(event), deleteEventBehaviour.delete(event));
    }

    @Test
    public void testShareReturnMethodFromShareBehaviour() {
        String extendedUrl = "this/" + userId;
        String expectedOutput = shareScheduleBehaviour.share(extendedUrl);
        assertEquals(feature.share(extendedUrl), expectedOutput);
    }

    @Test
    public void testFollowMethodSuccess() {
        ResponseEntity<Void> response = feature.follow(userId);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void testUpdateScheduleSuccess() {
        ResponseEntity<Void> response = feature.updateSchedule(userId);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void testSearchEventsOnSpecificDate() {
        String date = "2020-05-19";
        String extendedUrl = String.format("search/%s/%s", date, userId);
        assertEquals(feature.search(date, userId), listScheduleBehaviour.list(extendedUrl));
    }

    @Test
    public void testEditEvent() {
        String[] editedInput = { "TDD Search, Delete, all", "20:30", "23:30", "2020-05-19", "no" };
        JSONObject event = JsonCreator.createEvent(userId, editedInput);
        event.put("id", 15);
        List<Object> response = feature.edit(event);
        assertEquals(response, new ArrayList<>());
    }

}
