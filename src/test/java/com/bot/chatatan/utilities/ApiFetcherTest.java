package com.bot.chatatan.utilities;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class ApiFetcherTest {

    @Autowired
    RestTemplate testRestTemplate;

    @Autowired
    ApiFetcher fetch;

    @Before
    public void setUp() {
        fetch.setBaseUrl("schedule");
    }

    @Test
    public void testGetListSuccess() {
        String[] input = {"TDD Reach 100% coverage yok", "14:00", "15:00", "2020-06-04", "no"};
        JSONObject event = JsonCreator.createEvent("1806205666", input);
        fetch.post("event", event);

        fetch.getList("this/1806205666");
        String url = "http://scheduleapi/this/1806205666";
        ResponseEntity<List> response = testRestTemplate.getForEntity(url, List.class);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void testGetString() {
        String result = fetch.getString("details/this/1806205666");
        String url = "http://scheduleapi/details/this/1806205666";
        ResponseEntity<String> response = testRestTemplate.getForEntity(url, String.class);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertEquals(response.getBody(), result);
    }

    @Test
    public void testPostWithJsonRequestBody() {
        String[] input = {"TDD Json Creator", "20:30", "23:30", "2020-04-23", "no"};
        JSONObject event = JsonCreator.createEvent("1806205666", input);
        ResponseEntity<Void> response = fetch.post("event", event);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void testPostWithUrlOnly() {
        ResponseEntity<Void> response = fetch.post("follow/1806205666");
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void testPut() {
        String[] editedInput = {"TDD Search, Delete, all", "20:30", "23:30", "2020-05-19", "no"};
        JSONObject event = JsonCreator.createEvent("1806205666", editedInput);
        event.put("id", 15);
        ResponseEntity<List> response = fetch.put(event);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertEquals(response.getBody(), new ArrayList<>());
    }

    @Test
    public void testDelete() {
        String[] input = {"TDD Json Creator", "20:30", "23:30", "2020-04-23", "no"};
        JSONObject event = JsonCreator.createEvent("1806205666", input);
        event.put("id", 1);
        ResponseEntity<String> response = fetch.delete("event", event);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

}