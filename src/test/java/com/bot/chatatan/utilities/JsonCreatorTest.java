package com.bot.chatatan.utilities;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class JsonCreatorTest {

    JsonCreator jsonCreator;
    JSONObject eventJsonWithDate;
    JSONObject eventJsonWithDayNameOnly;
    String userId;

    @Before
    public void setUp() {
        userId = "1806205666";
        jsonCreator = new JsonCreator();

        eventJsonWithDate = new JSONObject();
        eventJsonWithDate.put("userId", userId);
        eventJsonWithDate.put("name", "TDD Json Creator");
        eventJsonWithDate.put("startTime", "20:30");
        eventJsonWithDate.put("endTime", "23:30");
        eventJsonWithDate.put("isRoutine", false);
        eventJsonWithDate.put("date", "2020-04-23");
        eventJsonWithDate.put("dayName", "THURSDAY");

        eventJsonWithDayNameOnly = new JSONObject();
        eventJsonWithDayNameOnly.put("userId", userId);
        eventJsonWithDayNameOnly.put("name", "Demo TK Adprog");
        eventJsonWithDayNameOnly.put("startTime", "15:00");
        eventJsonWithDayNameOnly.put("endTime", "17:40");
        eventJsonWithDayNameOnly.put("isRoutine", true);
        eventJsonWithDayNameOnly.put("dayName", "THURSDAY");
    }

    @Test
    public void testCreateEventAcceptDateInput() {
        String[] inputValues = {"TDD Json Creator", "20:30", "23:30", "2020-04-23", "no"};
        JSONObject result = JsonCreator.createEvent(userId, inputValues);
        JSONAssert.assertEquals(result, eventJsonWithDate, false);
    }

    @Test
    public void testCreateEventAcceptDayNameInput() {
        String[] inputValues = {"Demo TK Adprog", "15:00", "17:40", "thursday", "yes"};
        JSONObject result = JsonCreator.createEvent(userId, inputValues);
        JSONAssert.assertEquals(result, eventJsonWithDayNameOnly, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateEventWithIncorrectStartTimeFormat() {
        String[] inputValues = {"Demo TK Adprog", "1a:a0", "17:40", "thursday", "yes"};
        JsonCreator.createEvent(userId, inputValues);
    }

    @Test(expected = IllegalArgumentException.class)
    public  void testCreateEventWithIncorrectEndTimeFormat() {
        String[] inputValues = {"Demo TK Adprog", "15:00", "1a:a0", "thursday", "yes"};
        JsonCreator.createEvent(userId, inputValues);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateEventWithIllogicalTimeInput() {
        String[] inputValues = {"Demo TK Adprog", "17:40", "15:00", "thursday", "yes"};
        JsonCreator.createEvent(userId, inputValues);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateEventWithIncorrectDateFormat() {
        String[] inputValues = {"TDD Json Creator", "20:30", "23:30", "2020-4-23", "no"};
        JsonCreator.createEvent(userId, inputValues);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateEventWithIncorrectIsRoutineFormat() {
        String[] inputValues = {"TDD Json Creator", "20:30", "23:30", "2020-04-23", "NOT ROUTINE"};
        JsonCreator.createEvent(userId, inputValues);
    }
}
