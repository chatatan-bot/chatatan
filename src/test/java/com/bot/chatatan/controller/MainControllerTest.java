package com.bot.chatatan.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.bot.chatatan.flexcontainers.FeatureFlex;
import com.bot.chatatan.flexcontainers.FollowFlex;
import com.bot.chatatan.strategy.Feature;
import com.bot.chatatan.utilities.JsonCreator;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.FollowEvent;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.postback.PostbackContent;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class MainControllerTest {

    @Mock(name = "feature")
    private Feature feature;

    @Mock
    private LineMessagingClient client;

    @InjectMocks
    private MainController mainController;

    TextMessageContent textMessageContent;
    MessageEvent messageEvent;
    PostbackContent postbackContent;
    PostbackEvent postbackEvent;
    UserSource userSource;

    @Before
    public void setUp() throws Exception {
        userSource = UserSource.builder().userId("1806205666").build();
        MockitoAnnotations.initMocks(this);
        mainController = new MainController(client, feature);
    }

    @Test
    public void testHandleFollowEvent() throws Exception {
        FlexMessage followFlex = new FollowFlex().get();
        ReplyMessage reply = new ReplyMessage("replyToken", followFlex);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        FollowEvent followEvent = FollowEvent.builder()
                                            .replyToken("replyToken")
                                            .source(userSource)
                                            .build();
        mainController.handleFollowEvent(followEvent);
        verify(client).replyMessage(reply);
    }

    @Test
    public void testHandleDefaultPostback() throws Exception {
        String input = JsonCreator.createButton("BACK").toString();
        postbackContent = PostbackContent.builder().data(input).build();
        postbackEvent = PostbackEvent.builder()
                                    .replyToken("replyToken")
                                    .source(userSource)
                                    .postbackContent(postbackContent)
                                    .build();

        FlexMessage featureFlex = new FeatureFlex().get();
        ReplyMessage reply = new ReplyMessage("replyToken", featureFlex);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handlePostbackEvent(postbackEvent);
        verify(client).replyMessage(reply);    
    }

    @Test
    public void testHandleDefaultTextMessage() throws Exception {
        String input = "woi";
        textMessageContent = TextMessageContent.builder().text(input).build();
        messageEvent = MessageEvent.builder()
                                .replyToken("replyToken")
                                .source(userSource)
                                .message(textMessageContent)
                                .build();

        FlexMessage featureFlex = new FeatureFlex().get();
        ReplyMessage reply = new ReplyMessage("replyToken", featureFlex);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handleTextMessageEvent(messageEvent);
        verify(client).replyMessage(reply);
    }

    @Test
    public void testHandlePostbackException() throws Exception {
        when(feature.edit(any(JSONObject.class))).thenThrow(new RuntimeException());

        String[] inputValues = {"TDD Edit End Time Error", "14:30", "16:00", "SUNDAY", "no"};
        JSONObject event = JsonCreator.createEvent("1806205666", inputValues);

        Map<String, String> params = new HashMap<>();
        params.put("time", "12:00");

        String input = JsonCreator.createPostback("EDITENDTIME", event).toString();
        postbackContent = PostbackContent.builder()
                                        .data(input)
                                        .params(params)
                                        .build();
        postbackEvent = PostbackEvent.builder()
                                    .replyToken("replyToken")
                                    .source(userSource)
                                    .postbackContent(postbackContent)
                                    .build();

        String errorMessage = "Sorry, we are having technical issues. Please try again later.";
        ReplyMessage reply = new ReplyMessage("replyToken", new TextMessage(errorMessage));
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handlePostbackEvent(postbackEvent);
        verify(client).replyMessage(reply);    
    }

}