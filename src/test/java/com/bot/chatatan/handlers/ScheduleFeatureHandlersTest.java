package com.bot.chatatan.handlers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.bot.chatatan.controller.MainController;
import com.bot.chatatan.flexcontainers.EditEventFlex;
import com.bot.chatatan.flexcontainers.ScheduleCarousel;
import com.bot.chatatan.flexcontainers.ScheduleGuide;
import com.bot.chatatan.flexcontainers.SearchOutputFlex;
import com.bot.chatatan.flexcontainers.ShareScheduleFlex;
import com.bot.chatatan.strategy.Feature;
import com.bot.chatatan.utilities.JsonCreator;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.postback.PostbackContent;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class ScheduleFeatureHandlersTest {

    @Mock(name = "feature")
    private Feature feature;

    @Mock
    private LineMessagingClient client;

    @InjectMocks
    private MainController mainController;

    TextMessageContent textMessageContent;
    MessageEvent messageEvent;
    PostbackContent postbackContent;
    PostbackEvent postbackEvent;
    UserSource userSource;

    @Before
    public void setUp() throws Exception {
        userSource = UserSource.builder().userId("1806205666").build();
        MockitoAnnotations.initMocks(this);
        mainController = new MainController(client, feature);
        feature.setAddBehaviour("addToEvents");
        feature.setListBehaviour("listSchedule");
        feature.setDeleteBehaviour("deleteEvent");
        feature.setShareBehaviour("shareSchedule");
    }

    @Test
    public void testfeatureGuide() throws Exception {
        String input = JsonCreator.createButton("HELPSCHEDULE").toString();
        postbackContent = PostbackContent.builder().data(input).build();
        postbackEvent = PostbackEvent.builder()
                                    .replyToken("replyToken")
                                    .source(userSource)
                                    .postbackContent(postbackContent)
                                    .build();

        ReplyMessage reply = new ReplyMessage("replyToken", new ScheduleGuide().get());
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handlePostbackEvent(postbackEvent);
        verify(client).replyMessage(reply);    
    }

    @Test
    public void testHandleAddCommand() throws Exception {
        String input = "Add\nTDD Text Message Event\n01:00\n12:00\n2020-05-10\nNo";
        textMessageContent = TextMessageContent.builder().text(input).build();
        messageEvent = MessageEvent.builder()
                                .replyToken("replyToken")
                                .source(userSource)
                                .message(textMessageContent)
                                .build();

        TextMessage textMessage = new TextMessage("Noted! Your event has been saved :)");
        ReplyMessage reply = new ReplyMessage("replyToken", textMessage);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handleTextMessageEvent(messageEvent);
        verify(client).replyMessage(reply);
    }

    @Test
    public void testHandleListPostbackReturnScheduleCarousel() throws Exception {
        List<JSONObject> events = new ArrayList<>();
        String[] inputValues = {"TDD Main Controller", "00:30", "04:15", "2020-05-15", "no"};
        JSONObject event = JsonCreator.createEvent("1806205666", inputValues);
        events.add(event);
        String[] inputValues2 = {"TDD Schedule Carousel", "10:30", "11:15", "2020-05-16", "no"};
        event = JsonCreator.createEvent("1806205666", inputValues2);
        events.add(event);
        when(feature.list("this/1806205666")).thenReturn(events);

        String detail = "This week";
        String input = JsonCreator.createAction("LISTSCHEDULE", detail).toString();
        postbackContent = PostbackContent.builder().data(input).build();
        postbackEvent = PostbackEvent.builder()
                                    .replyToken("replyToken")
                                    .source(userSource)
                                    .postbackContent(postbackContent)
                                    .build();

        FlexMessage flexMessage = new ScheduleCarousel(events, detail).get();
        ReplyMessage reply = new ReplyMessage("replyToken", flexMessage);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handlePostbackEvent(postbackEvent);
        verify(client).replyMessage(reply);    
    }

    @Test
    public void testNextWeekScheduleCarousel() throws Exception {
        List<JSONObject> events = new ArrayList<>();
        String[] inputValues = {"TDD Next Week Schedule", "13:30", "14:15", "2020-06-04", "no"};
        JSONObject event = JsonCreator.createEvent("1806205666", inputValues);
        events.add(event);
        new ScheduleCarousel(events, "next").get(); 
    }

    @Test
    public void testHandleListPostbackWithNoEventsFound() throws Exception {
        List<JSONObject> events = new ArrayList<>();
        when(feature.list("this/1806205666")).thenReturn(events);
        
        String detail = "This week";
        String input = JsonCreator.createAction("LISTSCHEDULE", detail).toString();
        postbackContent = PostbackContent.builder().data(input).build();
        postbackEvent = PostbackEvent.builder()
                                    .replyToken("replyToken")
                                    .source(userSource)
                                    .postbackContent(postbackContent)
                                    .build();

        TextMessage textMessage = new TextMessage("No events found..");
        ReplyMessage reply = new ReplyMessage("replyToken", textMessage);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handlePostbackEvent(postbackEvent);
        verify(client).replyMessage(reply);    
    }

    @Test
    public void testHandleDeleteSchedulePostback() throws Exception {
        String[] inputValues = {"TDD Delete Feature", "22:00", "23:30", "2020-05-22", "no"};
        JSONObject event = JsonCreator.createEvent("1806205666", inputValues);

        JSONObject input = JsonCreator.createPostback("DELETESCHEDULE", event);
        postbackContent = PostbackContent.builder().data(input.toString()).build();
        postbackEvent = PostbackEvent.builder()
                                    .replyToken("replyToken")
                                    .source(userSource)
                                    .postbackContent(postbackContent)
                                    .build();

        String deleteResponse = "Sorry, the event you wish to edit doesn't exist :(";
        when(feature.delete(any(JSONObject.class))).thenReturn(deleteResponse);

        ReplyMessage reply = new ReplyMessage("replyToken", new TextMessage(deleteResponse));
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handlePostbackEvent(postbackEvent);
        verify(client).replyMessage(reply);    
    }

    @Test
    public void testEditSchedulePostback() throws Exception {
        String[] inputValues = {"TDD Edit Feature", "17:00", "19:30", "2020-05-23", "yes"};
        JSONObject event = JsonCreator.createEvent("1806205666", inputValues);
        event.put("isRoutine", "true");

        JSONObject input = JsonCreator.createPostback("EDITSCHEDULE", event);
        postbackContent = PostbackContent.builder().data(input.toString()).build();
        postbackEvent = PostbackEvent.builder()
                                    .replyToken("replyToken")
                                    .source(userSource)
                                    .postbackContent(postbackContent)
                                    .build();
     
        FlexMessage editFlex = new EditEventFlex(event).get();            
        ReplyMessage reply = new ReplyMessage("replyToken", editFlex);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handlePostbackEvent(postbackEvent);
        verify(client).replyMessage(reply);    
    }

    @Test
    public void testHandleEditStartTimePostback() throws Exception {
        String[] inputValues = {"TDD Edit Start Time", "12:30", "14:00", "SUNDAY", "no"};
        JSONObject event = JsonCreator.createEvent("1806205666", inputValues);
        event.put("isRoutine", "false");
        event.put("date", "");

        Map<String, String> params = new HashMap<>();
        params.put("time", "12:00");
        List<Object> events = new ArrayList<>();
        events.add(event);
        when(feature.edit(any(JSONObject.class))).thenReturn(events);

        String input = JsonCreator.createPostback("EDITSTARTTIME", event).toString();
        postbackContent = PostbackContent.builder()
                                        .data(input)
                                        .params(params)
                                        .build();
        postbackEvent = PostbackEvent.builder()
                                    .replyToken("replyToken")
                                    .source(userSource)
                                    .postbackContent(postbackContent)
                                    .build();

        JSONObject event2 = JsonCreator.createEvent("1806205666", inputValues);
        event2.put("isRoutine", "false");
        event2.put("date", "");
        FlexMessage flexMessage = new EditEventFlex(event2).get();
        ReplyMessage reply = new ReplyMessage("replyToken", flexMessage);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handlePostbackEvent(postbackEvent);
        verify(client).replyMessage(reply);    
    }

    @Test
    public void testHandleEditEndTimePostback() throws Exception {
        String[] inputValues = {"TDD Edit End Time", "12:30", "14:00", "SUNDAY", "no"};
        JSONObject event = JsonCreator.createEvent("1806205666", inputValues);
        event.put("isRoutine", "false");
        event.put("date", "");

        Map<String, String> params = new HashMap<>();
        params.put("time", "15:00");
        List<Object> events = new ArrayList<>();
        events.add(event);
        when(feature.edit(any(JSONObject.class))).thenReturn(events);

        String input = JsonCreator.createPostback("EDITENDTIME", event).toString();
        postbackContent = PostbackContent.builder()
                                        .data(input)
                                        .params(params)
                                        .build();
        postbackEvent = PostbackEvent.builder()
                                    .replyToken("replyToken")
                                    .source(userSource)
                                    .postbackContent(postbackContent)
                                    .build();

        JSONObject event2 = JsonCreator.createEvent("1806205666", inputValues);
        event2.put("isRoutine", "false");
        event2.put("date", "");
        FlexMessage flexMessage = new EditEventFlex(event2).get();
        ReplyMessage reply = new ReplyMessage("replyToken", flexMessage);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handlePostbackEvent(postbackEvent);
        verify(client).replyMessage(reply);    
    }

    @Test
    public void testHandleEditDatePostback() throws Exception {
        Map<String, String> params = new HashMap<>();
        params.put("date", "2020-05-24");

        String input = JsonCreator.createButton("EDITDATE").toString();
        postbackContent = PostbackContent.builder()
                                        .data(input)
                                        .params(params)
                                        .build();
        postbackEvent = PostbackEvent.builder()
                                    .replyToken("replyToken")
                                    .source(userSource)
                                    .postbackContent(postbackContent)
                                    .build();

        Message message = new TextMessage("Sorry, the event you wish to edit doesn't exist :(");
        ReplyMessage reply = new ReplyMessage("replyToken", message);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handlePostbackEvent(postbackEvent);
        verify(client).replyMessage(reply);    
    }

    @Test
    public void testHandleSearchPostBackReturnSearchOutputFlex() throws Exception {
        List<JSONObject> events = new ArrayList<>();
        String[] inputValues = {"TDD Search Feature", "09:30", "11:00", "2020-05-23", "no"};
        JSONObject event = JsonCreator.createEvent("1806205666", inputValues);
        events.add(event);

        Map<String, String> params = new HashMap<>();
        params.put("date", "2020-05-23");
        when(feature.search(params.get("date"), "1806205666")).thenReturn(events);

        String input = JsonCreator.createButton("SEARCH").toString();
        postbackContent = PostbackContent.builder()
                                        .data(input)
                                        .params(params)
                                        .build();
        postbackEvent = PostbackEvent.builder()
                                    .replyToken("replyToken")
                                    .source(userSource)
                                    .postbackContent(postbackContent)
                                    .build();

        FlexMessage flexMessage = new SearchOutputFlex(events, params.get("date")).get();
        ReplyMessage reply = new ReplyMessage("replyToken", flexMessage);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handlePostbackEvent(postbackEvent);
        verify(client).replyMessage(reply);    
    }

    @Test
    public void testHandleSearchPostbackReturnNoEventsFound() throws Exception {
        List<JSONObject> events = new ArrayList<>();
        Map<String, String> params = new HashMap<>();
        params.put("date", "2021-01-01");
        when(feature.search(params.get("date"), "1806205666")).thenReturn(events);

        String input = JsonCreator.createButton("SEARCH").toString();
        postbackContent = PostbackContent.builder()
                                        .data(input)
                                        .params(params)
                                        .build();
        postbackEvent = PostbackEvent.builder()
                                    .replyToken("replyToken")
                                    .source(userSource)
                                    .postbackContent(postbackContent)
                                    .build();

        TextMessage textMessage = new TextMessage("No events found..");
        ReplyMessage reply = new ReplyMessage("replyToken", textMessage);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handlePostbackEvent(postbackEvent);
        verify(client).replyMessage(reply);    
    }

    @Test
    public void testHandleIllegalArgumentException() throws Exception {
        String input = "Add\nTDD Text Message Event\n01:00\n12:00\n2020-05-10\nNope";
        String[] data = input.split("\\r?\\n");
        String[] values = Arrays.copyOfRange(data, 1, data.length);
        String errorMessage = "So, is it routine or nah?";
        when(feature.add("1806205666", values))
            .thenThrow(new IllegalArgumentException(errorMessage));

        textMessageContent = TextMessageContent.builder().text(input).build();
        messageEvent = MessageEvent.builder()
                                .replyToken("replyToken")
                                .source(userSource)
                                .message(textMessageContent)
                                .build();

        String message = errorMessage + "\n\nPlease resend it with the correct format.";
        ReplyMessage reply = new ReplyMessage("replyToken", new TextMessage(message));
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handleTextMessageEvent(messageEvent);
        verify(client).replyMessage(reply);
    }

    @Test
    public void testHandleIndexOutOfBoundsException() throws Exception {
        String input = "add";
        String[] data = input.split("\n");
        String[] values = Arrays.copyOfRange(data, 1, data.length);
        when(feature.add("1806205666", values)).thenThrow(new IndexOutOfBoundsException());

        textMessageContent = TextMessageContent.builder().text(input).build();
        messageEvent = MessageEvent.builder()
                                .replyToken("replyToken")
                                .source(userSource)
                                .message(textMessageContent)
                                .build();

        TextMessage textMessage = new TextMessage("The message you send is incomplete :(");
        ReplyMessage reply = new ReplyMessage("replyToken", textMessage);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handleTextMessageEvent(messageEvent);
        verify(client).replyMessage(reply);
    }

    @Test
    public void testHandleSharePostbackReturnShareScheduleFlex() throws Exception {
        String schedule = "FRIDAY\n11:00-12:00\nDEADLINE TK ADPROG AAA";
        when(feature.share("this/1806205666")).thenReturn(schedule);

        String detail = "This week";
        String input = JsonCreator.createAction("SHARESCHEDULE", detail).toString();
        postbackContent = PostbackContent.builder().data(input).build();
        postbackEvent = PostbackEvent.builder()
                                    .replyToken("replyToken")
                                    .source(userSource)
                                    .postbackContent(postbackContent)
                                    .build();

        FlexMessage flexMessage = new ShareScheduleFlex(schedule).get();
        ReplyMessage reply = new ReplyMessage("replyToken", flexMessage);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handlePostbackEvent(postbackEvent);
        verify(client).replyMessage(reply);    
    }

    @Test
    public void testHandleSharePostbackWithNoEventsFound() throws Exception {
        when(feature.share("this/1806205666")).thenReturn(null);
        
        String detail = "This week";
        String input = JsonCreator.createAction("SHARESCHEDULE", detail).toString();
        postbackContent = PostbackContent.builder().data(input).build();
        postbackEvent = PostbackEvent.builder()
                                    .replyToken("replyToken")
                                    .source(userSource)
                                    .postbackContent(postbackContent)
                                    .build();

        TextMessage textMessage = new TextMessage("No events found..");
        ReplyMessage reply = new ReplyMessage("replyToken", textMessage);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handlePostbackEvent(postbackEvent);
        verify(client).replyMessage(reply);    
    }

}
