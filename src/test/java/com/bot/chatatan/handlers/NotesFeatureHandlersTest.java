package com.bot.chatatan.handlers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.bot.chatatan.controller.MainController;
import com.bot.chatatan.flexcontainers.NotesCarousel;
import com.bot.chatatan.flexcontainers.NotesDetailsBubble;
import com.bot.chatatan.flexcontainers.NotesGuide;
import com.bot.chatatan.strategy.Feature;
import com.bot.chatatan.utilities.JsonCreator;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.postback.PostbackContent;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class NotesFeatureHandlersTest {

    @Mock(name = "feature")
    private Feature feature;

    @Mock
    private LineMessagingClient client;

    @InjectMocks
    private MainController mainController;

    TextMessageContent textMessageContent;
    MessageEvent messageEvent;
    PostbackContent postbackContent;
    PostbackEvent postbackEvent;
    UserSource userSource;

    @Before
    public void setUp() throws Exception {
        userSource = UserSource.builder().userId("1806205666").build();
        MockitoAnnotations.initMocks(this);
        mainController = new MainController(client, feature);
    }

    @Test
    public void testNotesFeatureGuide() throws Exception {
        String input = JsonCreator.createButton("HELPNOTES").toString();
        postbackContent = PostbackContent.builder().data(input).build();
        postbackEvent = PostbackEvent.builder()
                                    .replyToken("replyToken")
                                    .source(userSource)
                                    .postbackContent(postbackContent)
                                    .build();
        
        ReplyMessage reply = new ReplyMessage("replyToken", new NotesGuide().get());
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handlePostbackEvent(postbackEvent);
        verify(client).replyMessage(reply);    
    }

    @Test
    public void testHandleWriteCommand() throws Exception {
        String input = "Write\nTDD Write Text Message Event\nHope this works\nmore\nand more";
        textMessageContent = TextMessageContent.builder().text(input).build();
        messageEvent = MessageEvent.builder()
                                .replyToken("replyToken")
                                .source(userSource)
                                .message(textMessageContent)
                                .build();

        TextMessage textMessage = new TextMessage("Noted! Your notes has been saved :)");
        ReplyMessage reply = new ReplyMessage("replyToken", textMessage);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handleTextMessageEvent(messageEvent);
        verify(client).replyMessage(reply);
    }

    @Test
    public void testHandleIndexOutOfBoundsException() throws Exception {
        String input = "write";
        textMessageContent = TextMessageContent.builder().text(input).build();
        messageEvent = MessageEvent.builder()
                                .replyToken("replyToken")
                                .source(userSource)
                                .message(textMessageContent)
                                .build();

        TextMessage textMessage = new TextMessage("The message you send is incomplete :(");
        ReplyMessage reply = new ReplyMessage("replyToken", textMessage);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handleTextMessageEvent(messageEvent);
        verify(client).replyMessage(reply);
    }

    @Test
    public void testHandleListPostbackReturnNotesCarousel() throws Exception {
        List<JSONObject> notes = new ArrayList<>();
        String[] inputValues = {"WRITE", "test1", "hello there"};
        JSONObject note = JsonCreator.createNotes("1806205666", inputValues);
        for (int i = 0; i < 12; i++) {
            notes.add(note);
        }
        when(feature.list("1806205666")).thenReturn(notes);

        String input = JsonCreator.createButton("LISTNOTES").toString();
        postbackContent = PostbackContent.builder().data(input).build();
        postbackEvent = PostbackEvent.builder()
                                    .replyToken("replyToken")
                                    .source(userSource)
                                    .postbackContent(postbackContent)
                                    .build();

        FlexMessage flexMessage = new NotesCarousel(notes).get();
        ReplyMessage reply = new ReplyMessage("replyToken", flexMessage);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handlePostbackEvent(postbackEvent);
        verify(client).replyMessage(reply);    
    }

    @Test
    public void testHandleDetailPostbackReturnNotesDetailBubble() throws Exception {
        String[] inputValues = {"WRITE", "test1", "hello there"};
        JSONObject note = JsonCreator.createNotes("1806205666", inputValues);
        note.put("command", "DETAILSNOTES");
        note.put("id", "1");

        postbackContent = PostbackContent.builder().data(note.toString()).build();
        postbackEvent = PostbackEvent.builder()
                                    .replyToken("replyToken")
                                    .source(userSource)
                                    .postbackContent(postbackContent)
                                    .build();

        String shareOutput = "test1\nhello there";
        when(feature.share("1")).thenReturn(shareOutput);
        FlexMessage flexMessage = new NotesDetailsBubble(shareOutput, note).get();
        ReplyMessage reply = new ReplyMessage("replyToken", flexMessage);
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handlePostbackEvent(postbackEvent);
        verify(client).replyMessage(reply);    
    }
 
    @Test
    public void testHandleDeleteNotesPostback() throws Exception {
        String[] inputValues = {"WRITE", "test1", "hello there"};
        JSONObject note = JsonCreator.createNotes("1806205666", inputValues);

        JSONObject input = JsonCreator.createPostback("DELETENOTES", note);
        postbackContent = PostbackContent.builder().data(input.toString()).build();
        postbackEvent = PostbackEvent.builder()
                                    .replyToken("replyToken")
                                    .source(userSource)
                                    .postbackContent(postbackContent)
                                    .build();

        when(feature.delete(any(JSONObject.class))).thenReturn("deleteResponse");
        ReplyMessage reply = new ReplyMessage("replyToken", new TextMessage("deleteResponse"));
        when(client.replyMessage(reply))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("X-Line-Request-Id", "ok", Collections.emptyList())
        ));

        mainController.handlePostbackEvent(postbackEvent);
        verify(client).replyMessage(reply);    
    }
}
